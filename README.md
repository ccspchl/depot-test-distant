# Séance du 04/01/2022

## Mon premier dépôt Git
Ceci est mon premier dépôt.

## Liste des commandes

- 'git init' : initialise le dépôt
- 'git add' : ajoute un fichier à zone d'index
- 'git commit' : valide les modifications indexées dans la zone d'index
- 'git status' : voir l'état du dépôt
- 'git commit -am "blabla"' : '-a' ajoute au commit les fichiers modifiés, '-m' l'ajoute directement sans passer pas l'éditeur tmp
- 'git log' : affiche l'historique

# Séance du 05/01/2022

## Liste des commandes

- 'git remote add' : ajouter le dépôt comme remote
- 'git push' : "pousse" (copie) la branche locale actuelle sur la branh du remote
- 'git branch master --set-upstream-to=mon_depot_distant/master' : pour que la branche locale master suive automatiquement la branche master du remote mon_depot_distant, on ajuste besoin de faire git push mtn
- 'git clone [lien SSH ou HTTPS]' : permet de cloner un dépôt distant en local



